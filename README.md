# Experis Academy - Angular Pokemontrainer
This is the last assignement for the fronend period of the accelerate course on Noroff for Experis Academy.

## Usage
Clone/fork project and run `ng serve` to start. Solution open on `http://localhost:4200`.

## Features

There are three main parent components: register, trainer and catalogue. Both the catalogue and the trainer page uses the pokemon-detail component as a child to show the details the details of a pokemon. The solution is showing how to send props down to a child and how to emmit an event to the parent.
<br>
There are services to send api calls to the pokemon api and to sort out the session. When a user logges out, the user object in localstorage gets deleted and the cached list is updated so the collected flag is set to false on the previously collected pokemons.
<br><br>
Additional features beyond the assignement itself are:
* Caching of pokemons
* Sorting posibilities on the Trainer page