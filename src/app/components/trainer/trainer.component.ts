import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service';
import { Trainer } from 'src/app/models/trainer.model';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {

  public trainer: Trainer;
  public storedPokemons: Pokemon[];
  public pokemonList: Pokemon[] = [];

  public isShowingDetail: boolean = false;
  public pokemonToShow: Pokemon;

  public prevSort: string = 'id';
  public sortDirection: string = "arrow_circle_up";

  constructor(private session: SessionService) { }

  ngOnInit(): void {
    this.trainer = this.session.get();
    this.storedPokemons = this.session.getPokemon();
    this.trainer.pokemon.forEach(e=>this.pokemonList.push(this.storedPokemons[e]));
    this.pokemonList.sort((a,b)=>a.id-b.id);
  }

  onPokemonClicked(id): void {
    console.log(id);
    this.pokemonToShow = this.storedPokemons[id];
    this.isShowingDetail = true;
  }

  closeDetail(e): void {
    this.isShowingDetail = false;
  }

  sortArray(method: string): void {
    switch (method) {
      case 'id':
        if(this.prevSort != 'id'){
          this.pokemonList.sort((a,b)=>a.id-b.id);
          this.sortDirection = 'arrow_circle_up';
        }else{
          this.pokemonList.reverse();
          if(this.sortDirection == 'arrow_circle_up'){
            this.sortDirection = 'arrow_circle_down';
          }else{
            this.sortDirection = 'arrow_circle_up';
          }
        }
        this.prevSort = 'id';
        break;
      case 'name':
        if(this.prevSort != 'name'){
          this.pokemonList.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
          this.sortDirection = 'arrow_circle_up';
        }else{
          this.pokemonList.reverse();
          if(this.sortDirection == 'arrow_circle_up'){
            this.sortDirection = 'arrow_circle_down';
          }else{
            this.sortDirection = 'arrow_circle_up';
          }
        }
        this.prevSort = 'name';
        break;
      default:
        this.pokemonList.sort((a,b)=>a.id-b.id);
        break;
    }
  }

}
