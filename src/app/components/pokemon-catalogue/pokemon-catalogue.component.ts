import { Component, OnInit, OnDestroy } from '@angular/core';
import { PokemonApiService } from '../../services/pokemon-api/pokemon-api.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.css']
})
export class PokemonCatalogueComponent implements OnInit, OnDestroy {

  public currentPokemons: Pokemon[] = [];
  public storedPokemons: Pokemon[];
  public currentDetailPokemon: Pokemon;

  public nextUrl: string = '';
  public previousUrl: string = '';

  public isLoading: boolean = false;
  public isShowingDetail: boolean = false;

  constructor(private pokemonApi: PokemonApiService, private session: SessionService) { }

  ngOnInit(): void {
    this.storedPokemons = this.session.getPokemon();
    this.getPokemonPage(true);
  }

  ngOnDestroy(): void {
    this.session.savePokemon(this.storedPokemons);
  }

  async getPokemonPage(isFirst: boolean, url?: string) {
    this.isLoading = true;
    try {
      this.currentPokemons = [];
      let result: any;
      if (isFirst) {
        result = await this.pokemonApi.getFirstPokemonPage();
      } else {
        result = await this.pokemonApi.getRequestByUrl(url);
      }
      this.nextUrl = result.next;
      this.previousUrl = result.previous;
      result.results.forEach(async e => {
        try {
          const pokemonUrlSplit: string[] = e.url.split('/');
          const pokemonId: number = parseInt(pokemonUrlSplit[pokemonUrlSplit.length - 2]);
          if (pokemonId > 5000) {
            this.nextUrl = '';
            return;
          }
          if (typeof this.storedPokemons[pokemonId] !== 'undefined' && this.storedPokemons[pokemonId] !== null) {
            //Pokemon exists
            this.currentPokemons.push(this.storedPokemons[pokemonId]);
          } else {
            //Pokemon dosnt exist
            const loopingPokemon: any = await this.pokemonApi.getRequestByUrl(e.url);
            //Generating object for current looping pokemon
            let pokemonToSave: Pokemon = {
              id: loopingPokemon.id,
              collected: false,
              name: loopingPokemon.name,
              image: loopingPokemon.sprites.front_default,
              types: [...loopingPokemon.types].map(e => e.type.name),
              base_stats: [
                ...loopingPokemon.stats.map(e => ({
                  base_stat: e.base_stat,
                  name: e.stat.name
                }))
              ],
              height: loopingPokemon.height,
              weight: loopingPokemon.weight,
              abilities: [...loopingPokemon.abilities].map(e => e.ability.name),
              base_experience: loopingPokemon.base_experience,
              moves: [...loopingPokemon.moves].map(e => e.move.name)
            };

            this.currentPokemons.push(pokemonToSave)
            this.storedPokemons[pokemonId] = pokemonToSave;
          }

          //Sort list by pokemon id
          this.currentPokemons.sort((a, b) => a.id - b.id);

        } catch (e) {
          console.error(`TC error: ${e.message}`);
        }
      });

    } catch (e) {
      console.error(`TC error: ${e.message}`);
    } finally {
      this.isLoading = false;
    }
    console.log(this.currentPokemons);
  }


  nextBtnClicked(): void {
    if (this.nextUrl !== '' || this.nextUrl !== null) {
      this.getPokemonPage(false, this.nextUrl);
    }
  }

  previousBtnClicked(): void {
    if (this.previousUrl !== '' || this.previousUrl !== null) {
      this.getPokemonPage(false, this.previousUrl);
    }
  }

  pokemonClicked(e): void {
    this.isShowingDetail = true;
    this.currentDetailPokemon = this.storedPokemons[e];
    console.log("Pokemon to send: ", this.currentDetailPokemon);
  }

  closeDetail(e): void {
    this.isShowingDetail = false;
  }

  collectPokemon(e): void {
    if (!this.storedPokemons[e].collected) {
      this.session.addPokemonToTrainer(e);
      this.storedPokemons[e].collected = true;
    }
  }

}
