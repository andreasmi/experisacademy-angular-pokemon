import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionService } from '../../services/session/session.service';
import { Trainer } from '../../models/trainer.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public isLoading: boolean = false;
  public newTrainer: Trainer = {
    name: '',
    pokemon: []
  }

  public registerForm: FormGroup = new FormGroup({
    trainername: new FormControl('', 
    [Validators.required,
    Validators.minLength(3), 
    Validators.maxLength(20)])
  });

  constructor(private session: SessionService, private router: Router) {
    if (this.session.get() !== null) {
      this.router.navigateByUrl('/trainer');
    }
  }

  ngOnInit(): void {
  }

  get trainername() { return this.registerForm.get('trainername'); }

  onRegisterClciked() {
    this.isLoading = true;
    this.newTrainer.name = this.registerForm.value.trainername;
    this.session.save(this.newTrainer);
    this.isLoading = false;
    this.router.navigateByUrl('/trainer');
  }

}
