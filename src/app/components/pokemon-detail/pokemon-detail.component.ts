import { Component, OnInit, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit, OnChanges {

  @Input() pokemon;
  @Output() closeDetail: EventEmitter<any> = new EventEmitter();
  @Output() collectPokemon: EventEmitter<number> = new EventEmitter();

  public isNotUndefined: boolean = false;

  constructor() { }

  ngOnInit(): void {
    console.log("current pokemon detail: ", this.pokemon, typeof this.pokemon);

  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("Changes: ", changes)
    if(typeof changes.pokemon.currentValue !== 'undefined'){
      this.isNotUndefined = true;
      this.pokemon = changes.pokemon.currentValue;
      console.log(this.pokemon.base_stats)

    }else{
      this.isNotUndefined = false;
    }
  }

  public onCloseDetail(e): void{
    this.closeDetail.emit( e );
  }

  public collectClicked(): void{
    this.collectPokemon.emit( this.pokemon.id );
  }

}
