import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './components/register/register.component';
import { TrainerComponent } from './components/trainer/trainer.component';
import { PokemonCatalogueComponent } from './components/pokemon-catalogue/pokemon-catalogue.component';
import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'register',
    component: RegisterComponent
  },{
    path: 'trainer',
    component: TrainerComponent,
    canActivate: [ AuthGuard ]
  },{
    path: 'catalogue',
    component: PokemonCatalogueComponent,
    canActivate: [ AuthGuard ]
  },{
    path: '',
    pathMatch: 'full',
    redirectTo: '/register'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
