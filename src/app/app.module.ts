//Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

//Components
import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { TrainerComponent } from './components/trainer/trainer.component';
import { PokemonCatalogueComponent } from './components/pokemon-catalogue/pokemon-catalogue.component';
import { PokemonDetailComponent } from './components/pokemon-detail/pokemon-detail.component';
import { SiteHeaderComponent } from './shared/site-header/site-header.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    TrainerComponent,
    PokemonCatalogueComponent,
    PokemonDetailComponent,
    SiteHeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
