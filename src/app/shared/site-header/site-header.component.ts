import { Component, OnDestroy } from '@angular/core';
import { SessionService } from '../../services/session/session.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-site-header',
  templateUrl: './site-header.component.html',
  styleUrls: ['./site-header.component.css']
})
export class SiteHeaderComponent implements OnDestroy {

  private header$: Subscription;
  public isAuth: boolean;

  constructor(private session: SessionService, private router: Router, private location: Location) { 
    this.header$ = router.events.subscribe(e=>{
      if(location.path() != "" && location.path() != "/register"){
        this.isAuth = true;
      }else{
        this.isAuth = false;
      }
    })
  }

  ngOnDestroy(): void{
    this.header$.unsubscribe();
  }

  logoutClicked(): void {
    this.session.delete();
    this.router.navigateByUrl("");
  }
  

}
