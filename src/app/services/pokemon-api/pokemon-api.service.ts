import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Pokemon } from '../../models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonApiService {

  constructor(private http: HttpClient) { }

  getFirstPokemonPage(): Promise<any>{
    return this.http.get(`${environment.baseApiUrl}pokemon/?limit=30`).toPromise();
  }

  getRequestByUrl(url: string): Promise<any>{
    return this.http.get(url).toPromise();
  }

}
