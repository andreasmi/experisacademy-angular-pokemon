import { Injectable } from '@angular/core';
import { Trainer } from '../../models/trainer.model'
import { Pokemon } from 'src/app/models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  save(trainer: Trainer) {
    localStorage.setItem('trainer', JSON.stringify(trainer));
    if(localStorage.getItem('previousSessionCollected')){
      let pokemons: Pokemon[] = JSON.parse(localStorage.getItem('pokemons'));
      JSON.parse(localStorage.getItem('previousSessionCollected')).forEach(e => pokemons[e].collected = false);
      this.savePokemon(pokemons);
    }
  }
  get(): Trainer {
    const session = localStorage.getItem('trainer');
    return session ? {...JSON.parse(session)} : null;
  }
  delete(): void {
    if(localStorage.getItem('trainer')){
      localStorage.setItem('previousSessionCollected', JSON.stringify(JSON.parse(localStorage.getItem('trainer')).pokemon));
    }
    localStorage.removeItem('trainer');    
  }
  addPokemonToTrainer(pokemonId: Pokemon): void{
    let trainer: any = JSON.parse(localStorage.getItem('trainer'));
    const collected: Pokemon[] = trainer.pokemon;
    if(!collected.includes(pokemonId)){
      collected.push(pokemonId);
      trainer.pokemon = collected;
      localStorage.setItem('trainer', JSON.stringify(trainer));
    }
  }
  savePokemon(pokemons: Pokemon[]): void {
    localStorage.setItem('pokemons', JSON.stringify(pokemons));
  }
  getPokemon(): Pokemon[] {
    const savedPokemons = localStorage.getItem('pokemons');
    return savedPokemons ? {...JSON.parse(savedPokemons)} : [];
  }
}