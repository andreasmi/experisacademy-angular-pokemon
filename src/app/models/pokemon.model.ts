import { Stat } from './stat.model';

export interface Pokemon {
    //Pokemon model
    id: number;
    collected: boolean;

    //Base stats
    name: string;
    image: string;
    types: string[];
    base_stats: Stat[];

    //Profile
    height: number;
    weight: number;
    abilities: string[];
    base_experience: number;

    //Moves
    moves: string[];

}